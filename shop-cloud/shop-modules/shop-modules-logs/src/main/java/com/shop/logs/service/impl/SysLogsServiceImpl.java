package com.shop.logs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.core.domain.entity.SysLogs;
import com.shop.logs.service.SysLogsService;
import com.shop.logs.mapper.SysLogsMapper;
import org.springframework.stereotype.Service;

/**
* @author dada
* @description 针对表【sys_logs】的数据库操作Service实现
* @createDate 2024-07-12 15:52:25
*/
@Service
public class SysLogsServiceImpl extends ServiceImpl<SysLogsMapper, SysLogs>
    implements SysLogsService{

}




