package com.shop.logs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.core.domain.entity.SysLogs;

/**
* @author dada
* @description 针对表【sys_logs】的数据库操作Mapper
* @createDate 2024-07-12 15:52:25
* @Entity com.shop.logs.domain.SysLogs
*/
public interface SysLogsMapper extends BaseMapper<SysLogs> {

}




