package com.shop.logs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.shop.core.domain.entity.SysLogs;

/**
* @author dada
* @description 针对表【sys_logs】的数据库操作Service
* @createDate 2024-07-12 15:52:25
*/
public interface SysLogsService extends IService<SysLogs> {

}
