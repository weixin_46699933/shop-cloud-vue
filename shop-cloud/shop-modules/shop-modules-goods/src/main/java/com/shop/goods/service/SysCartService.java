package com.shop.goods.service;

import com.shop.core.domain.entity.SysCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author dada
* @description 针对表【sys_cart】的数据库操作Service
* @createDate 2024-07-12 16:01:23
*/
public interface SysCartService extends IService<SysCart> {

}
