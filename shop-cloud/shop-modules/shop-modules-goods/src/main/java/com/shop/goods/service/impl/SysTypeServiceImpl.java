package com.shop.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.core.domain.entity.SysType;
import com.shop.goods.service.SysTypeService;
import com.shop.goods.mapper.SysTypeMapper;
import org.springframework.stereotype.Service;

/**
* @author dada
* @description 针对表【sys_type】的数据库操作Service实现
* @createDate 2024-07-12 16:01:23
*/
@Service
public class SysTypeServiceImpl extends ServiceImpl<SysTypeMapper, SysType>
    implements SysTypeService{

}




