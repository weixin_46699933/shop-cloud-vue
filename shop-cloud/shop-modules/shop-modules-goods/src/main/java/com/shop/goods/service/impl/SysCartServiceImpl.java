package com.shop.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.core.domain.entity.SysCart;
import com.shop.goods.service.SysCartService;
import com.shop.goods.mapper.SysCartMapper;
import org.springframework.stereotype.Service;

/**
* @author dada
* @description 针对表【sys_cart】的数据库操作Service实现
* @createDate 2024-07-12 16:01:23
*/
@Service
public class SysCartServiceImpl extends ServiceImpl<SysCartMapper, SysCart>
    implements SysCartService{

}




