package com.shop.goods.service;

import com.shop.core.domain.entity.SysGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author dada
* @description 针对表【sys_goods】的数据库操作Service
* @createDate 2024-07-12 13:11:39
*/
public interface SysGoodsService extends IService<SysGoods> {

}
