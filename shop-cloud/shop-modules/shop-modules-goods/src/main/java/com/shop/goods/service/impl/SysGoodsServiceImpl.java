package com.shop.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.core.domain.entity.SysGoods;
import com.shop.goods.service.SysGoodsService;
import com.shop.goods.mapper.SysGoodsMapper;
import org.springframework.stereotype.Service;

/**
* @author dada
* @description 针对表【sys_goods】的数据库操作Service实现
* @createDate 2024-07-12 13:11:39
*/
@Service
public class SysGoodsServiceImpl extends ServiceImpl<SysGoodsMapper, SysGoods>
    implements SysGoodsService{

}




