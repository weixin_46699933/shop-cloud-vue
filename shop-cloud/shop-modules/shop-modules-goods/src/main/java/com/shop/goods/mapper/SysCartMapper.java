package com.shop.goods.mapper;

import com.shop.core.domain.entity.SysCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author dada
* @description 针对表【sys_cart】的数据库操作Mapper
* @createDate 2024-07-12 16:01:23
* @Entity com.shop.core.domain.entity.SysCart
*/
public interface SysCartMapper extends BaseMapper<SysCart> {

}




