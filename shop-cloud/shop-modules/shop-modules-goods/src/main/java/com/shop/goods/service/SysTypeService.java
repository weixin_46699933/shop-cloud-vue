package com.shop.goods.service;

import com.shop.core.domain.entity.SysType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author dada
* @description 针对表【sys_type】的数据库操作Service
* @createDate 2024-07-12 16:01:23
*/
public interface SysTypeService extends IService<SysType> {

}
