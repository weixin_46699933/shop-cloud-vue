package com.shop.goods.mapper;

import com.shop.core.domain.entity.SysGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author dada
* @description 针对表【sys_goods】的数据库操作Mapper
* @createDate 2024-07-12 13:11:39
* @Entity com.shop.core.domain.entity.SysGoods
*/
public interface SysGoodsMapper extends BaseMapper<SysGoods> {

}




