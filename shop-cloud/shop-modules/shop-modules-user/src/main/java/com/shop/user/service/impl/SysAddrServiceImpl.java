package com.shop.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.shop.core.domain.entity.SysAddr;
import com.shop.user.service.SysAddrService;
import com.shop.user.mapper.SysAddrMapper;
import org.springframework.stereotype.Service;

/**
* @author dada
* @description 针对表【sys_addr】的数据库操作Service实现
* @createDate 2024-07-12 15:57:34
*/
@Service
public class SysAddrServiceImpl extends ServiceImpl<SysAddrMapper, SysAddr>
    implements SysAddrService{

}




