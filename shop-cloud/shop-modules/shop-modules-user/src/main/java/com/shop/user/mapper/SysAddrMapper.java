package com.shop.user.mapper;

import com.shop.core.domain.entity.SysAddr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author dada
* @description 针对表【sys_addr】的数据库操作Mapper
* @createDate 2024-07-12 15:57:34
* @Entity com.shop.core.domain.entity.SysAddr
*/
public interface SysAddrMapper extends BaseMapper<SysAddr> {

}




