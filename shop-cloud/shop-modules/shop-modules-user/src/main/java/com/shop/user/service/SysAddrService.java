package com.shop.user.service;

import com.shop.core.domain.entity.SysAddr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author dada
* @description 针对表【sys_addr】的数据库操作Service
* @createDate 2024-07-12 15:57:34
*/
public interface SysAddrService extends IService<SysAddr> {

}
