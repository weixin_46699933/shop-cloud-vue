package com.shop.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.core.domain.entity.SysUser;

/**
* @author dada
* @description 针对表【sys_user】的数据库操作Mapper
* @createDate 2024-07-11 15:12:39
* @Entity com.shop.user.domain.SysUser
*/
public interface SysUserMapper extends BaseMapper<SysUser> {

}




