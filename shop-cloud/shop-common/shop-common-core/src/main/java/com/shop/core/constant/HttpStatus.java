package com.shop.core.constant;

/**
 * @Author: dada
 * @Date: 2024/7/10 22:41
 * @Version: 1.0
 * @Description:
 */
public interface HttpStatus {

    int SUCCESS = 200;

    int ERROR = 500;
    /**
     * 未授权
     */
    int UNAUTHORIZED = 401;

}
