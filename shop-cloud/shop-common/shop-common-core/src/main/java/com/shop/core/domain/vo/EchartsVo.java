package com.shop.core.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: dada
 * @Date: 2024/8/11 23:21
 * @Version: 1.0
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EchartsVo {
    private Integer value;
    private String name;
}
