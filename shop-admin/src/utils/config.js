import logo from '@/assets/favicon.ico';

/**
 * 哒哒开发
 * https://www.itzd.cn
 */
export default {
    logo: logo,
    name: '商城',
    title: '环保商城后台',
    // 纵向和横向菜单时，菜单字体和logo颜色
    textColor: 'rgb(255,255,255)',
    //纵向菜单时，昵称颜色
    textHeaderColor: 'rgb(84,84,84)',
    // 菜单背景颜色
    backgroundColor: 'rgb(41,66,86)',
    // 纵向菜单时，顶部背景颜色
    backHeadergroundColor: 'rgb(255,255,255)',
    // 菜单激活时颜色
    activeTextColor: 'rgb(0,160,255)',
    titleStyle: {
        margin: '40px',
        color: 'aliceblue'
    }
};
